import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './guard/login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'users',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'users',
    loadChildren: () => import('./user/user.module').then( m => m.UserPageModule),
    //canActivate: [LoginGuard]
  },
  {
    path: 'foto',
    loadChildren: () => import('./foto/foto.module').then( m => m.FotoPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'load-image',
    loadChildren: () => import('./load-image/load-image.module').then( m => m.LoadImagePageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'pdf-viewer',
    loadChildren: () => import('./pdf-viewer/pdf-viewer.module').then( m => m.PdfViewerPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'qr-code-generator',
    loadChildren: () => import('./qr-code-generator/qr-code-generator.module').then( m => m.QrCodeGeneratorPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'bar-code-generator',
    loadChildren: () => import('./bar-code-generator/bar-code-generator.module').then( m => m.BarCodeGeneratorPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'qr-code-scanner',
    loadChildren: () => import('./qr-code-scanner/qr-code-scanner.module').then( m => m.QrCodeScannerPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'storage-set',
    loadChildren: () => import('./storage-set/storage-set.module').then( m => m.StorageSetPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'storage-get',
    loadChildren: () => import('./storage-get/storage-get.module').then( m => m.StorageGetPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'preferences-get',
    loadChildren: () => import('./preferences-get/preferences-get.module').then( m => m.PreferencesGetPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'preferences-set',
    loadChildren: () => import('./preferences-set/preferences-set.module').then( m => m.PreferencesSetPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./logout/logout.module').then( m => m.LogoutPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
