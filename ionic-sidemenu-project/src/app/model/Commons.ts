export interface SelectItemValue{
  label: string,
  value: string
};

export interface FotoInfo{
  path?: string
  extension?: string,
  filename?: string,
  realPath?: string
};
