import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Inbox', url: '/folder/inbox', icon: 'mail' },
    { title: 'Usuarios', url: '/users', icon: 'person' },
    { title: 'Tomar foto', url: '/foto', icon: 'camera' },
    { title: 'Cargar imagen', url: '/load-image', icon: 'image' },
    { title: 'Ver PDF', url: '/pdf-viewer', icon: 'document' },
    { title: 'Generate QR Code', url: '/qr-code-generator', icon: 'qr-code' },
    { title: 'Generate Bar Code', url: '/bar-code-generator', icon: 'barcode' },
    { title: 'Leer QR Code', url: '/qr-code-scanner', icon: 'qr-code' },
    { title: 'Guardar en Storage', url: '/storage-set', icon: 'save' },
    { title: 'Leer de Storage', url: '/storage-get', icon: 'save' },
    { title: 'Guardar preferencias', url: '/preferences-set', icon: 'save' },
    { title: 'Leer preferencias', url: '/preferences-get', icon: 'save' },
    { title: 'Logout', url: '/logout', icon: 'log-out' }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
