import { Component, OnInit } from '@angular/core';
import { Barcode, BarcodeScanner } from '@capacitor-mlkit/barcode-scanning';

@Component({
  selector: 'app-qr-code-scanner',
  templateUrl: './qr-code-scanner.page.html',
  styleUrls: ['./qr-code-scanner.page.scss'],
})
export class QrCodeScannerPage implements OnInit {

  isSuported : boolean = false;
  showMessage : boolean;
  message: string;
  barCodes : Barcode[] = [];

  constructor() {
    this.showMessage = false;
    this.message = "";
  }

  ngOnInit() {
    BarcodeScanner.isSupported()
      .then(result => {
        this.isSuported = result.supported;

        if(!this.isSuported){
          this.showMessage = true;
          this.message = "QR Scanner No soportado";
        }
      })
  }

  async scanQrCode(){
    const havePermission : boolean = await this.requestPermission();

    if(havePermission){
      const {barcodes} = await BarcodeScanner.scan();

      this.barCodes.push(...barcodes);
    }else{
      this.showMessage = true;
      this.message = "No tiene permisos para usar QR Scanner";
    }
  }

  private async requestPermission() : Promise<boolean>{
    const {camera} = await BarcodeScanner.requestPermissions();

    if(camera === 'limited' || camera === 'granted'){
      return true;
    }else{
      return false;
    }
  }

  setOpen(flag: boolean) : void{
    this.showMessage = flag;
  }

}
