export class Utils {

  public async getBase64FromUri(uri : string){
    const response = await fetch(uri);
    const blob = await response.blob();

    return await this.convertBlobToBase64(blob) as string;
  }

  public convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });
}
