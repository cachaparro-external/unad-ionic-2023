import { ComponentFixture, TestBed } from '@angular/core/testing';
import { QrCodeGeneratorPage } from './qr-code-generator.page';

describe('QrCodeGeneratorPage', () => {
  let component: QrCodeGeneratorPage;
  let fixture: ComponentFixture<QrCodeGeneratorPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(QrCodeGeneratorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
