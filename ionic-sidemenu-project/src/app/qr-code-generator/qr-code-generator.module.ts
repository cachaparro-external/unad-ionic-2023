import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrCodeGeneratorPageRoutingModule } from './qr-code-generator-routing.module';

import { QrCodeGeneratorPage } from './qr-code-generator.page';
import { QrCodeModule } from 'ng-qrcode';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    QrCodeGeneratorPageRoutingModule,
    QrCodeModule
  ],
  declarations: [QrCodeGeneratorPage]
})
export class QrCodeGeneratorPageModule {}
