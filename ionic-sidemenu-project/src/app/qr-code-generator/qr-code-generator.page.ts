import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-qr-code-generator',
  templateUrl: './qr-code-generator.page.html',
  styleUrls: ['./qr-code-generator.page.scss'],
})
export class QrCodeGeneratorPage implements OnInit {

  texto : FormControl;
  qrCode : any;

  constructor() {
    this.texto = new FormControl("", [Validators.required], []);
  }

  ngOnInit() {
  }

  capturarQrCode() : void{
    this.qrCode = this.texto.value;
  }

  limpiarQrCode() : void{
    this.qrCode = null;
  }

}
