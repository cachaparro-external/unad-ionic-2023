import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserInfo } from '../model/UserInfo';
import { Preferences } from '@capacitor/preferences';
import { USER_INFO_PROP } from '../util/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  form : FormGroup;
  showMessage : boolean;
  message: string;

  constructor(private loginService : LoginService,
    private formBuilder : FormBuilder,
    private router : Router) {
    this.form = this.formBuilder.group({
      usernameField: ["", [Validators.required], []],
      passwordField: ["", [Validators.required], []]
    });

    this.showMessage = false;
    this.message = "";
  }

  ngOnInit() {
  }

  doLogin() : void{
    console.log("Haciendo login con usuario: " + this.form.value.usernameField);

    this.loginService.login(this.form.value.usernameField, this.form.value.passwordField)
      .subscribe({
        next: async (response) => {
          let user : UserInfo = response;

          await Preferences.set({
            key: USER_INFO_PROP,
            value: JSON.stringify(user),
          });

          this.message = "Autenticación exitosa";
          this.showMessage = true;

          this.router.navigate(["/"]);
        },
        error: (error) => {
          if(error.status === 401){
            this.message = "Usuario o clave incorrecta";
            this.showMessage = true;
          }else{
            console.log("Error " + JSON.stringify(error));
            this.message = "Ocurrio un error";
            this.showMessage = true;
          }
        }
    });
  }

  setOpen(flag: boolean) : void{
    this.showMessage = flag;
  }
}
