import { Component, OnInit } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { USER_INFO_PROP } from '../util/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
    /*await Preferences.get({
      key: USER_INFO_PROP
    }).then(values => {
      console.log("Values before: " + JSON.stringify(values))
    });*/

    Preferences.remove({
      key: USER_INFO_PROP
    }).then(() => {
      this.router.navigate(["/login"]);
    });

    /*await Preferences.get({
      key: USER_INFO_PROP
    }).then(values => {
      console.log("Values: " + JSON.stringify(values))
    });*/

  }

}
