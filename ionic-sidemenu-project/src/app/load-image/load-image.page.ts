import { Component, OnInit } from '@angular/core';
import { ImageService } from '../service/image.service';
import { DownloadFileService } from '../service/download-file.service';
import { Utils } from '../util/utils';
import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';

@Component({
  selector: 'app-load-image',
  templateUrl: './load-image.page.html',
  styleUrls: ['./load-image.page.scss'],
})
export class LoadImagePage implements OnInit {

  showMessage : boolean;
  message: string;
  image : string | undefined;

  constructor(private imageService : ImageService,
              private downloadService : DownloadFileService) {
    this.showMessage = false;
    this.message = "";
  }

  ngOnInit() {
  }

  cargarImagen(){
    this.imageService.loadImage("MiFoto.png")
    .then(result => {
      this.image = result;
    }).catch(error => {
      this.message = "Por favor primero cargue la imagen";
      this.showMessage = true;
    });
  }

  borrarImagen(){
    this.imageService.deleteImage("MiFoto.png")
    .then(() => {
      this.image = undefined;
      this.message = "Imagen borrada exitosamente";
      this.showMessage = true;
    }).catch(() => {
      this.message = "No existe imagen para borrar";
      this.showMessage = true;
    });
  }

  descargarImagen(){
    const util : Utils = new Utils();

    this.downloadService
      .downloadFile("https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Ionic-logo-landscape.svg/1200px-Ionic-logo-landscape.svg.png")
      .subscribe(blob => {
        console.log("Archivo descargado: " + blob);
        util.convertBlobToBase64(blob)
          .then(base64 => {
            const fotoPath = "misFotos/MiFoto.png";
            const base64String : string = base64 as string;

            Filesystem.writeFile({
              path: fotoPath,
              data: base64String,
              directory: Directory.Documents,
              encoding: Encoding.UTF8
            }).then(result => {
              this.cargarImagen();
            }).catch(error => {
              console.log(error);
              this.message = "Fallo al guardar el archivo";
              this.showMessage = true;
            });
          }).catch(error => {
            console.log(error);
            this.message = "Ocurrio un error al descargar el archivo";
            this.showMessage = true;
          });
      });
  }

  setOpen(flag: boolean) : void{
    this.showMessage = flag;
  }

}
