import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoadImagePage } from './load-image.page';

describe('LoadImagePage', () => {
  let component: LoadImagePage;
  let fixture: ComponentFixture<LoadImagePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LoadImagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
