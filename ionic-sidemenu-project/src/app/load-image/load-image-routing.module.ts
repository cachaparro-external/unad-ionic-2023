import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoadImagePage } from './load-image.page';

const routes: Routes = [
  {
    path: '',
    component: LoadImagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoadImagePageRoutingModule {}
