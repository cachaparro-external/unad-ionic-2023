import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoadImagePageRoutingModule } from './load-image-routing.module';

import { LoadImagePage } from './load-image.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoadImagePageRoutingModule
  ],
  declarations: [LoadImagePage]
})
export class LoadImagePageModule {}
