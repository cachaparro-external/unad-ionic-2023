import { Injectable } from '@angular/core';
import { SelectItemValue } from '../model/Commons';

@Injectable({
  providedIn: 'root'
})
export class ParamService {

  constructor() { }

  getTiposId() : Array<SelectItemValue>{
    const tiposId : Array<SelectItemValue> = [];

    tiposId.push({
      label: "Cédula de ciudadanía",
      value: "CC"
    });

    tiposId.push({
      label: "Tarjeta de identidad",
      value: "TI"
    });

    tiposId.push({
      label: "NUIP",
      value: "NUIP"
    });

    tiposId.push({
      label: "Pasaporte",
      value: "PAS"
    });

    return tiposId;
  }

}
