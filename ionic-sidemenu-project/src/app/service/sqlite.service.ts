import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { CapacitorSQLite, SQLiteConnection, SQLiteDBConnection, capSQLiteChanges } from '@capacitor-community/sqlite';
import { Capacitor } from '@capacitor/core';
import { USERS_DB_NAME } from '../util/constants';

@Injectable({
  providedIn: 'root'
})
export class SqliteService implements OnInit, OnDestroy{

  sqliteConnection : SQLiteConnection | undefined;
  native : boolean = false;

  constructor() { }

  ngOnInit(): void {

  }

  init() : Promise<boolean>{
    const platform : string = Capacitor.getPlatform();

    console.log("Platform: " + platform);

    if(platform == "android" || platform == "ios"){
      this.native = true;

      let sqlitePlugin = CapacitorSQLite;
      this.sqliteConnection = new SQLiteConnection(sqlitePlugin);

      return Promise.resolve(true);
    }else{
      //Para Web IndexesDB
      return Promise.reject(false);
      //throw new Error("Plataforma " + platform + " no soportada" );
    }
  }

  public async createConnection(readOnly : boolean) : Promise<SQLiteDBConnection>{
    if(this.checkDatabaseConnection()){
      const conn : SQLiteDBConnection = await this.sqliteConnection!.createConnection(USERS_DB_NAME, false, 'no-encryption', 1, readOnly);

      if(conn == null){
        throw new Error("Error al obtener la conexión a BD");
      }

      return conn;
    }else{
      throw new Error("SQLiteConnection es nulo");
    }
  }

  public async closeConnection(){
    if(this.checkDatabaseConnection()){
      this.sqliteConnection!.closeConnection(USERS_DB_NAME, false);
    }
  }

  public async createDatabaseModel(){
    const sql : string = `CREATE TABLE IF NOT EXISTS USERS(
      ID INTEGER PRIMARY KEY NOT NULL,
      FIRST_NAME TEXT NOT NULL,
      LAST_NAME TEXT NOT NULL,
      GENDER TEXT NOT NULL
    );`;

    try{
      const dbConn : SQLiteDBConnection = await this.createConnection(false);

      await dbConn.open();

      const dbResult : capSQLiteChanges = await dbConn.execute(sql);

      console.log("Creación de esquema: " + dbResult.changes?.changes);

      if(dbResult.changes!.changes! <= 0){
        throw new Error("No se creo correctamente el esquema");
      }
    }finally{
      await this.closeConnection();
    }
  }

  private checkDatabaseConnection() : boolean{
    return this.native && this.sqliteConnection !== undefined;
  }

  ngOnDestroy(): void {
    this.closeConnection();
  }
}
