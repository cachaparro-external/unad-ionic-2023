import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class IonicStorageService {
  private _storage : Storage | null = null;

  constructor(private storage : Storage) {
    this.init();
  }

  async init(){
    const storage = await this.storage.create();
    this._storage = storage;
  }

  async putData(key : string, value : any){
    this._storage?.set(key, value);
  }

  async getData(key : string){
    const esta : boolean = await this.containsKey(key);

    if(esta){
      return await this._storage?.get(key);
    }else{
      return null;
    }
  }

  async containsKey(key : string){
    const value : any = await this._storage?.get(key);

    return (value ? true : false);
  }

  async removeKey(key : string){
    await this._storage?.remove(key);
  }

  async clear(){
    await this._storage?.clear();
  }
}
