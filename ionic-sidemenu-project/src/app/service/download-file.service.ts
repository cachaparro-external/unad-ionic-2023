import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DownloadFileService {

  constructor(private httpClient : HttpClient) { }

  downloadFile(urlFile : string) : Observable<Blob>{
    return this.httpClient.get(urlFile, {
      responseType: "blob"
    });
  }
}
