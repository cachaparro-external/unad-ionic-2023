import { Injectable, OnInit } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { FotoInfo } from '../model/Commons';
import { Utils } from '../util/utils';
import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';

@Injectable({
  providedIn: 'root'
})
export class CamaraService implements OnInit{

  private MAIN_FOLDER : string = "misFotos";

  private foto : FotoInfo | undefined;

  constructor() {

  }

  ngOnInit(): void {
    Filesystem.mkdir({
      path: this.MAIN_FOLDER,
      directory: Directory.Documents,
      recursive: true
    });
  }

  public async capturarFoto(){
    const foto = await Camera.getPhoto({
      quality: 100,
      resultType: CameraResultType.DataUrl,
      //resultType: CameraResultType.Uri,
      width: 400,
      height: 200,
      source: CameraSource.Camera
    });

    this.foto = {
      path: foto.dataUrl,
      //path: foto.webPath,
      extension: foto.format
    };

    console.log(this.foto.path);
    console.log(this.foto.extension);
    console.log(await this.getFotoEnBase64());
  }

  public async leerDeGaleria(){
    const foto = await Camera.getPhoto({
      resultType: CameraResultType.DataUrl,
      //resultType: CameraResultType.Uri,
      source: CameraSource.Photos
    });

    this.foto = {
      path: foto.dataUrl,
      //path: foto.webPath,
      extension: foto.format
    };

    console.log(this.foto.path);
    console.log(this.foto.extension);
    console.log(await this.getFotoEnBase64());
  }

  public getFoto() :  FotoInfo | undefined{
    return this.foto;
  }

  public async getFotoEnBase64(){
    if(this.foto){
      if(this.foto.path!.startsWith("data")){
        //return this.foto.path!.replace('data:', '').replace(/^.+,/, '');
        return this.foto.path!;
      }else{
        const utils : Utils = new Utils();

        //utils.getBase64FromUri((this.foto.path ? this.foto.path : ""));
        return await utils.getBase64FromUri(this.foto.path!);
      }
    }

    return undefined;
  }

  public async guardarFoto(filename : string){
    if(this.foto){
      const newFilename : string = this.MAIN_FOLDER.concat("/").concat(filename).concat(".").concat(this.foto.extension!);
      const fileBase64 : string | undefined = await this.getFotoEnBase64();

      //console.log("Filename: " + newFilename);
      //console.log("Base64: " + fileBase64);

      /*const checkPermissions = await Filesystem.checkPermissions();

      console.log("checkPermissions: " + JSON.stringify(checkPermissions));

      const requestPermissions = Filesystem.requestPermissions();

      console.log("requestPermissions: " + JSON.stringify(requestPermissions));*/

      const archivoGuardado = await Filesystem.writeFile({
        path: newFilename,
        data: fileBase64!,
        directory: Directory.Documents,
        encoding: Encoding.UTF8,
        recursive: true
      });

      console.log("URI: " + archivoGuardado.uri);

      /*this.foto = {
        extension: this.foto.extension,
        path: this.foto.path,
        filename: newFilename,
        realPath: archivoGuardado.uri
      };*/

      //Incorrecto
      //this.foto.path = "";

      this.foto = {
        ...this.foto,
        filename: newFilename,
        realPath: archivoGuardado.uri
      };
    }
  }
}
