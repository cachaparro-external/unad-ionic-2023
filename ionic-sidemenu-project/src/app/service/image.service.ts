import { Injectable } from '@angular/core';
import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor() { }

  async loadImage(filename : string){
    const fullPath = "misFotos/".concat(filename);

    const contents = await Filesystem.readFile({
      path: fullPath,
      directory: Directory.Documents,
      encoding: Encoding.UTF8,
    }).catch(error => {
      console.log("Error en loadImage " + error);

      return Promise.reject();
    });

    console.log('Contenido: ', contents.data);

    return Promise.resolve(contents.data);
  }

  async deleteImage(filename : string){
    const fullPath = "misFotos/".concat(filename);

    await Filesystem.deleteFile({
      path: fullPath,
      directory: Directory.Documents,
    }).catch(error => {
      Promise.reject();
    });

    return Promise.resolve();
  }
}
