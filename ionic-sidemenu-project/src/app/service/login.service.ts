import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserInfo } from '../model/UserInfo';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private serviceUrl : string;

  constructor(private httpClient : HttpClient) {
    this.serviceUrl = "http://127.0.0.1:8080/api/security/login";
  }

  public login(username : string, password : string) : Observable<UserInfo>{
    const request : any = {
      username: username,
      password: password
    };

    return this.httpClient.post<UserInfo>(this.serviceUrl, request);
  }
}
