import { Component, OnInit } from '@angular/core';
import { CamaraService } from '../service/camara.service';
import { FotoInfo } from '../model/Commons';

@Component({
  selector: 'app-foto',
  templateUrl: './foto.page.html',
  styleUrls: ['./foto.page.scss'],
})
export class FotoPage implements OnInit {

  showMessage : boolean;
  message: string;

  constructor(private camaraService : CamaraService) {
    this.showMessage = false;
    this.message = "";
  }

  ngOnInit() {
  }

  tomarFoto() : void{
    this.camaraService.capturarFoto();
  }

  leerFoto() : void{
    this.camaraService.leerDeGaleria();
  }

  getFoto() : FotoInfo | undefined{
    return this.camaraService.getFoto();
  }

  guardarFoto() : void{
    if(this.camaraService.getFoto()){
      this.camaraService.guardarFoto("MiFoto");
    }else{
      this.message = "Por favor tome o cargue una foto";
      this.showMessage = true;
    }
  }

  setOpen(flag: boolean) : void{
    this.showMessage = flag;
  }

}
