import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PreferencesSetPage } from './preferences-set.page';

describe('PreferencesSetPage', () => {
  let component: PreferencesSetPage;
  let fixture: ComponentFixture<PreferencesSetPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PreferencesSetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
