import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreferencesSetPageRoutingModule } from './preferences-set-routing.module';

import { PreferencesSetPage } from './preferences-set.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    PreferencesSetPageRoutingModule
  ],
  declarations: [PreferencesSetPage]
})
export class PreferencesSetPageModule {}
