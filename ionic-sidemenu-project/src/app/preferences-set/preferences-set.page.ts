import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Preferences } from '@capacitor/preferences';
import { NAME_PROP } from '../util/constants';

@Component({
  selector: 'app-preferences-set',
  templateUrl: './preferences-set.page.html',
  styleUrls: ['./preferences-set.page.scss'],
})
export class PreferencesSetPage implements OnInit {

  nombre : FormControl;

  constructor() {
    this.nombre = new FormControl("", [Validators.required], []);
  }

  ngOnInit() {
  }

  async setNombre(){
    await Preferences.set({
      key: NAME_PROP,
      value: this.nombre.value,
    });
  }

  async clear(){
    await Preferences.remove({
      key: NAME_PROP
    });
  }

}
