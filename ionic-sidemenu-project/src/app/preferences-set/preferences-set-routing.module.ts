import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreferencesSetPage } from './preferences-set.page';

const routes: Routes = [
  {
    path: '',
    component: PreferencesSetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreferencesSetPageRoutingModule {}
