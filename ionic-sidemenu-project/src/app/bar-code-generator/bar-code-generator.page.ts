import { UserInfo } from './../model/UserInfo';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Preferences } from '@capacitor/preferences';
import * as JsBarcode from 'jsbarcode';
import { USER_INFO_PROP } from '../util/constants';

@Component({
  selector: 'app-bar-code-generator',
  templateUrl: './bar-code-generator.page.html',
  styleUrls: ['./bar-code-generator.page.scss'],
})
export class BarCodeGeneratorPage implements OnInit {

  texto : FormControl;
  showBarCode : boolean = false;
  name : string;

  constructor() {
    this.texto = new FormControl("", [Validators.required], []);
    this.name = "";
  }

  ngOnInit() {
    Preferences.get({
      key: USER_INFO_PROP
    }).then(values => {
      if(values && values.value){
        let user : UserInfo = JSON.parse(values.value);
        this.name = user.name;
      }
    });
  }

  generarBarCode() : void{
    JsBarcode("#barcode", this.texto.value);
    this.showBarCode = true;
  }

  limpiarBarCode() : void{
    this.showBarCode = false;
  }

}
