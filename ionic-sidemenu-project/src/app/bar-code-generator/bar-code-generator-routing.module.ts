import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BarCodeGeneratorPage } from './bar-code-generator.page';

const routes: Routes = [
  {
    path: '',
    component: BarCodeGeneratorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BarCodeGeneratorPageRoutingModule {}
