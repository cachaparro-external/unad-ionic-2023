import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BarCodeGeneratorPageRoutingModule } from './bar-code-generator-routing.module';

import { BarCodeGeneratorPage } from './bar-code-generator.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    BarCodeGeneratorPageRoutingModule
  ],
  declarations: [BarCodeGeneratorPage]
})
export class BarCodeGeneratorPageModule {}
