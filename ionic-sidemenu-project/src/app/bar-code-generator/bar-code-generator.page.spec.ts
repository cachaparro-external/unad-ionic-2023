import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BarCodeGeneratorPage } from './bar-code-generator.page';

describe('BarCodeGeneratorPage', () => {
  let component: BarCodeGeneratorPage;
  let fixture: ComponentFixture<BarCodeGeneratorPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(BarCodeGeneratorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
