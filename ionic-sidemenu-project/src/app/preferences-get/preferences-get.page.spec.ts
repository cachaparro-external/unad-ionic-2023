import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PreferencesGetPage } from './preferences-get.page';

describe('PreferencesGetPage', () => {
  let component: PreferencesGetPage;
  let fixture: ComponentFixture<PreferencesGetPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PreferencesGetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
