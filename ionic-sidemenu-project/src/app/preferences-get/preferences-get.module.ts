import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreferencesGetPageRoutingModule } from './preferences-get-routing.module';

import { PreferencesGetPage } from './preferences-get.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreferencesGetPageRoutingModule
  ],
  declarations: [PreferencesGetPage]
})
export class PreferencesGetPageModule {}
