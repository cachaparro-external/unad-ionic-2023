import { Component, OnInit } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { AlertController } from '@ionic/angular';
import { NAME_PROP } from '../util/constants';

@Component({
  selector: 'app-preferences-get',
  templateUrl: './preferences-get.page.html',
  styleUrls: ['./preferences-get.page.scss'],
})
export class PreferencesGetPage implements OnInit {

  constructor(private alert : AlertController) { }

  async ngOnInit() {
    const { value } = await Preferences.get({ key: NAME_PROP });

    if(value){
      const alertWindows = await this.alert.create({
        header: "Ionic Preferences",
        message: "Nombre: ".concat(value),
        buttons: ["OK"]
      });

      await alertWindows.present();
    }else{
      const alertWindows = await this.alert.create({
        header: "Ionic Preferences",
        message: "La preferencia Nombre no ha sido fijada",
        buttons: ["OK"]
      });

      await alertWindows.present();
    }
  }

}
