import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreferencesGetPage } from './preferences-get.page';

const routes: Routes = [
  {
    path: '',
    component: PreferencesGetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreferencesGetPageRoutingModule {}
