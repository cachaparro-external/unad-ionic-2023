import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StorageGetPageRoutingModule } from './storage-get-routing.module';

import { StorageGetPage } from './storage-get.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StorageGetPageRoutingModule
  ],
  declarations: [StorageGetPage]
})
export class StorageGetPageModule {}
