import { Component, OnInit } from '@angular/core';
import { IonicStorageService } from '../service/ionic-storage.service';
import { NAME_PROP } from '../util/constants';
import { AlertController } from '@ionic/angular';
import { pipe } from 'rxjs';

@Component({
  selector: 'app-storage-get',
  templateUrl: './storage-get.page.html',
  styleUrls: ['./storage-get.page.scss'],
})
export class StorageGetPage implements OnInit {
  mensaje : string = "";
  constructor(
    private storageService : IonicStorageService,
    private alert : AlertController) { }

  async ngOnInit() {
    const esta : boolean = await this.storageService.containsKey(NAME_PROP);

    if(esta){
      const value : any = await this.storageService.getData(NAME_PROP);

      //Con await
      const alertWindows = await this.alert.create({
        header: "Ionic Storage",
        message: "Nombre: ".concat(value),
        buttons: ["OK"]
      });

      await alertWindows.present();

      //Sin await
      /*this.alert.create({
        header: "Ionic Storage",
        message: "Nombre: ".concat(value),
        buttons: ["OK"]
      }).then(async alertWindow => {
        await alertWindow.present();
        this.mensaje = "";

        //Leer nombre
        Storage.save(Archivo)
          .then(result => {
            //Mostrar de exito
            this.mensaje
          })
      });*/

      //Con Pipe
      //pipe(pedirNombreArchivo(), guardarArchovo(), mopstrarMensaje());
    }else{
      const alertWindows = await this.alert.create({
        header: "Ionic Storage",
        message: "La propiedad Nombre no ha sido fijada",
        buttons: ["OK"]
      });

      await alertWindows.present();
    }
  }
}
