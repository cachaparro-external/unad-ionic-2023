import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StorageGetPage } from './storage-get.page';

describe('StorageGetPage', () => {
  let component: StorageGetPage;
  let fixture: ComponentFixture<StorageGetPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StorageGetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
