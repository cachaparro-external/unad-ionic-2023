import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StorageGetPage } from './storage-get.page';

const routes: Routes = [
  {
    path: '',
    component: StorageGetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StorageGetPageRoutingModule {}
