import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ParamService } from '../service/param.service';
import { SelectItemValue } from '../model/Commons';
import { SqliteService } from '../service/sqlite.service';
import { SQLiteDBConnection, capSQLiteChanges } from '@capacitor-community/sqlite';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {

  form : FormGroup;
  tiposId: Array<SelectItemValue>;
  showMessage : boolean;
  message: string;
  dbReady : boolean = true;

  constructor(private formBuilder : FormBuilder,
      private paramService : ParamService,
      private sqliteService : SqliteService) {
    this.form = this.formBuilder.group({
      nombres: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(10)], []],
      apellidos: ["", [Validators.required, Validators.minLength(3)], []],
      genero: ["", [Validators.required], []],
      email: ["", [Validators.required, Validators.email, Validators.pattern("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")], []],
      tipoId: ["", [Validators.required], []]
    });

    this.tiposId = this.paramService.getTiposId();

    this.showMessage = false;
    this.message = "";
  }

  ngOnInit() {
    this.sqliteService.init()
      .then(result => {
        this.dbReady = true;
        this.sqliteService.createDatabaseModel();
    }).catch(err => {
        this.dbReady = false;
    });
  }

  async onSubmit(){
    if(this.dbReady){
      this.sqliteService.createDatabaseModel();

      console.log("Nombres: " + this.form.value.nombres);
      console.log("Apellidos: " + this.form.value.apellidos);
      console.log("Genero: " + this.form.value.genero);
      console.log("Email: " + this.form.value.email);
      console.log("Tipo de ID: " + this.form.value.tipoId);

      try{
        const dbConn : SQLiteDBConnection = await  this.sqliteService.createConnection(false);

        dbConn.open();

        const dbValues : any[] = [
          this.form.value.nombres,
          this.form.value.apellidos,
          this.form.value.genero
        ];

        const dbResult : capSQLiteChanges = await dbConn.run(
          "INSERT INTO USERS (FIRST_NAME, LAST_NAME, GENDER) VALUES (?, ?, ?)",
          dbValues
        );

        if(dbResult.changes!.changes! >= 1){
          //Logica para guardar el usuario  --> Service
          this.message = "Usuario creado exitosamente con ID: " + dbResult.changes?.lastId;
          this.showMessage = true;
        }else{
          this.showMessage = true;
          this.message = "Ocurrio un error al guardar la información";
        }

        dbConn.close();

        this.clearForm();
      }catch(err){
        this.showMessage = true;
        this.message = "Ocurrio un error: " + err;
      }finally{
        await this.sqliteService.closeConnection();
      }
    }else{
      this.showMessage = true;
      this.message = "No hay base de datos local disponible";
    }
  }

  setOpen(flag: boolean) : void{
    this.showMessage = flag;
  }

  clearForm() : void{
    this.form.reset();
  }
}
