import { Component, OnInit } from '@angular/core';
import { DownloadFileService } from '../service/download-file.service';
import { Utils } from '../util/utils';
import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.page.html',
  styleUrls: ['./pdf-viewer.page.scss'],
})
export class PdfViewerPage implements OnInit {

  private PDF_URL : string = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";

  pdf : any;
  showMessage : boolean;
  message: string;

  constructor(private downloadService : DownloadFileService) {
    /*this.pdf = {
      url: this.PDF_URL
    }*/

    this.showMessage = false;
    this.message = "";

    this.downloadFile();
    //this.showFile();
   }

  ngOnInit() {
  }

  showFileInBrowser(){
    Browser.open({
      url: this.PDF_URL
    });
  }

  showFile(){
    const filePath = "misArchivos/pdf-test.pdf";

    Filesystem.readFile({
      path: filePath,
      directory: Directory.Documents,
      encoding: Encoding.UTF8
    }).then(result => {
      const base64String = result.data.replace('data:', '').replace(/^.+,/, '');
      //console.log("File: " + base64String);
      this.pdf = {
        data: atob(base64String)
      }
    }).catch(error => {
      console.log(error);
      this.message = "Primero descargue el archivo";
      this.showMessage = true;
    });
  }

  showFileFromUrl(){
    this.pdf = {
      url: this.PDF_URL
    }
  }

  downloadFile(){
    const util : Utils = new Utils();

    this.downloadService.downloadFile(this.PDF_URL)
      .subscribe(blob => {
        util.convertBlobToBase64(blob)
          .then(base64 => {
            const filePath = "misArchivos/pdf-test.pdf";
            const base64String : string = base64 as string;

            Filesystem.writeFile({
              path: filePath,
              data: base64String,
              directory: Directory.Documents,
              encoding: Encoding.UTF8
            }).catch(error => {
              console.log(error);
            });
          }).catch(error => {
            console.log(error);
          });
      });
  }

  setOpen(flag: boolean) : void{
    this.showMessage = flag;
  }

}
