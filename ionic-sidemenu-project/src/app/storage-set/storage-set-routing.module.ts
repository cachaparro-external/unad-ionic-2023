import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StorageSetPage } from './storage-set.page';

const routes: Routes = [
  {
    path: '',
    component: StorageSetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StorageSetPageRoutingModule {}
