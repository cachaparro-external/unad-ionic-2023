import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StorageSetPageRoutingModule } from './storage-set-routing.module';

import { StorageSetPage } from './storage-set.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    StorageSetPageRoutingModule
  ],
  declarations: [StorageSetPage]
})
export class StorageSetPageModule {}
