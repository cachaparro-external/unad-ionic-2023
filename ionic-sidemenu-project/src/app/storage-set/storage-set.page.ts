import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { IonicStorageService } from '../service/ionic-storage.service';
import { NAME_PROP } from '../util/constants';

@Component({
  selector: 'app-storage-set',
  templateUrl: './storage-set.page.html',
  styleUrls: ['./storage-set.page.scss'],
})
export class StorageSetPage implements OnInit {

  nombre : FormControl;

  constructor(private storageService : IonicStorageService) {
    this.nombre = new FormControl("", [Validators.required], []);
   }

  ngOnInit() {
  }

  setNombre() : void {
    this.storageService.putData(NAME_PROP, this.nombre.value);
  }

  clear() : void{
    this.storageService.removeKey(NAME_PROP);
  }

}
