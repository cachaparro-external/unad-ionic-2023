import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StorageSetPage } from './storage-set.page';

describe('StorageSetPage', () => {
  let component: StorageSetPage;
  let fixture: ComponentFixture<StorageSetPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StorageSetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
