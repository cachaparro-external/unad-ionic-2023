import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Preferences } from '@capacitor/preferences';
import { Observable } from 'rxjs';
import { USER_INFO_PROP } from '../util/constants';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private router : Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return Preferences.get({ key: USER_INFO_PROP })
      .then(values => {
        if(values && values.value){
          return Promise.resolve(true);
        }else{
          this.router.navigate(["/login"]);
          return Promise.resolve(false);
        }
      });
  }

}
