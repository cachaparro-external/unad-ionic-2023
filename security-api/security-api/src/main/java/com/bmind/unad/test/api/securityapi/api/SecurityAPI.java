package com.bmind.unad.test.api.securityapi.api;

import javax.annotation.PostConstruct;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/security")
@CrossOrigin("*")
public class SecurityAPI {
    
    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> login(
        @RequestBody(required = true) LoginRequestDTO dto
    ){
        try{
            Assert.notNull(dto, "LoginRequestDTO is null");
            Assert.hasText(dto.getUsername(), "Username is mandatory");
            Assert.hasText(dto.getPassword(), "Password is mandatory");

            if(dto.getUsername().equals(dto.getPassword())){
                LoginResponseDTO response = new LoginResponseDTO();
                response.setName("Rodrigo Martinez");
                
                return ResponseEntity.ok(response);
            }else{
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }catch(IllegalArgumentException e){
            return ResponseEntity.badRequest().build();
        }catch(Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }
}
